#!/usr/bin/env python3

#
# Python3 battleship sample bot implementation.
# https://bitbucket.org/tsapelman/battleship-server/
#

import random

# Main loop
while True:

    # Receive server request and reply to server depending on the request
    request = input()

    # Send ships allocation
    if request == 'Prepare':
        print('Ready A1-D1,F1-H1,J1-J3,J5-J6,J8-J9,H10-G10,E10-E10,C10,A10,A8')

    # Send a quadrant to be attacked
    elif request == 'Fire':
        print( "%s%d" % (chr(random.randrange(ord('A'), ord('J'))), random.randrange(1, 10)) )

    # Game over. Exit the program
    elif request in ['Win', 'Loss', 'Draw']:
        break

    # Ignore other requests
    else:
        pass
