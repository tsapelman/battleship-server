(*
 * Freepascal battleship sample bot implementation.
 * https://bitbucket.org/tsapelman/battleship-server/
 *)

program samplebot;

uses sysutils, dateutils;

var
	request : string;

begin
    // Initialize a random seed directly since randomize() procedure does it too roughly
    RandSeed := MilliSecondOfTheMonth(Now());

    // Main loop
    while True do begin

        // Receive server request and reply to server depending on the request
        readln(request);

        // Send ships allocation
        if request = 'Prepare' then
            writeln('Ready A1-D1,F1-H1,J1-J3,J5-J6,J8-J9,H10-G10,E10-E10,C10,A10,A8')

        // Send a quadrant to be attacked
        else if request = 'Fire' then
            writeln( chr(random(10)+ord('A')), random(10)+1 )

        // Game over. Exit the program
        else if (request = 'Win') or (request = 'Loss') or (request = 'Draw') then
            break

        // Ignore other requests
        else
            ;
    end
end.
